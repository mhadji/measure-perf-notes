#include <iostream>
#include <vector>
#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#define NBACCESS 1000000
#define NBR 5242880

unsigned int cpt[10];

using type = double;

int main() {
    std::cout << sizeof(type) << std::endl;
    std::cout << sizeof(type*) << std::endl;
    std::vector<type> vf(NBR);

    srand(time(NULL));
    /*
      for(auto& pf : vf) {
      pf = new type;
      }
    */
    /*
      for(unsigned int i=0; i < vf.size()-1; i++) {
      printf("[%d] :: %p - %p = %dB\n", i, &vf[i+1], &vf[i], (char*)&vf[i+1] - (char*)&vf[i]);
      }


      for(unsigned int i=0; i < vf.size()-1; i++) {
      printf("[%d] :: %p - %p = %dB\n", i, vf[i+1], vf[i], (char*)vf[i+1] - (char*)vf[i]);
      }
    */

    for(unsigned int k = 0; k < NBR/2; k++) {
        vf[k] = 8.;
    }

    /*
      #define BUFF 100000
      int c=0, d=0;
      while(true) {
      unsigned int nb[BUFF];

      #pragma omp parallel for num_threads(64)
      for(unsigned int i=0; i < BUFF; i++) {
      nb[i]= int(log(rand()%NBR));
      cpt[ nb[i] ]++;
      }

      std::cout << "\r";

      for(unsigned int j = 0; j < 10; j++)
      std::cout << cpt[j] << " ; ";

      std::cout << std::flush;
      }
    */

    return 0;
}
