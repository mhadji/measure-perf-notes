#!/bin/sh

make clean && make b_cpi
mv b_cpi b_cpi_noi

make OPT=-DIMPROVE_CPI
mv b_cpi b_cpi_i


amplxe-cl -collect general-exploration -q -- ./b_cpi_i
printf "\n---IMPROVE CPI---\n"
amplxe-cl -report hotspots -r r002ge/ -column='cpi' -S='cpi' -filter="function=main" -limit=10 -q

amplxe-cl -collect general-exploration -q -- ./b_cpi_noi
printf "\n---NO IMPROVE CPI---\n"
amplxe-cl -report hotspots -r r003ge/ -column='cpi' -S='cpi' -filter="function=main" -limit=10 -q

rm -rf r002ge r003ge
