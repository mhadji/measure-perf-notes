#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "System.hpp"

//#include "rdtsc.h"

#define SIZE (1 << 28)
#define NBR 100000
#define MSIZE 10000

int main() {
    srand(time(NULL));

    unsigned long long value;
    int cpt1 = 0;

    System::profile("loading", [&]() {
            for(int rpt = 0; rpt < NBR; ++rpt) {
                value = rand() % SIZE;

                if(!(value % 2)) {
                    cpt1 = cpt1*3 + 1;
                }
                else
                    cpt1 /= 2;
            }
        });

    printf("%d\n", cpt1);
    return 0;
}

/*
int main() {
    srand(time(NULL));
    reset_tsc();

    int ma[MSIZE][MSIZE], mb[MSIZE][MSIZE], mc[MSIZE][MSIZE];

    unsigned long long value;
    int cpt1 = 0;
    for(int rpt = 0; rpt < NBR; ++rpt) {
        value = rdtsc() % SIZE;

        if(!(value & 0x1)) {
            cpt1 = cpt1*3 + 1;
        }
        else
            cpt1 /= 2;

        for(int i = 0; i < MSIZE; i++)
            for(int j = 0; j < MSIZE; j++)
                for(int k = 0; k < MSIZE; k++)
                    mc[i][j] += ma[i][k] * mb[k][j];

        value = rdtsc() % SIZE;

        if(!(value & 0x1)) {
            cpt1 = cpt1*3 + 1;
        }
        else
            cpt1 /= 2;

        for(int i = 0; i < MSIZE; i++)
            for(int j = 0; j < MSIZE; j++)
                for(int k = 0; k < MSIZE; k++)
                    mc[i][j] += ma[i][k] * mb[k][j];

        value = rdtsc() % SIZE;

        if(!(value & 0x1)) {
            cpt1 = cpt1*3 + 1;
        }
        else
            cpt1 /= 2;

        for(int i = 0; i < MSIZE; i++)
            for(int j = 0; j < MSIZE; j++)
                for(int k = 0; k < MSIZE; k++)
                    mc[i][j] += ma[i][k] * mb[k][j];

    }

    printf("%d\n", cpt1);
    return 0;
}
*/
