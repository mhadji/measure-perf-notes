#include <iostream>
#include <valgrind/callgrind.h>
#ifdef INTELLINKED
#include "ittnotify.h"
#endif

int __attribute__ ((noinline)) add() {
    int val = 0;
    for(int i = 0; i < 800; ++i)
        val += i;
    return val;
}

int __attribute__ ((noinline)) mult() {
  int val = 1;
  for(int i = 0; i < 800; ++i)
    val *= i;
  return val;
}

int foo() {
    return add() + add() + mult();
}

int main(void) {
    int dum = 0;

    CALLGRIND_START_INSTRUMENTATION;
#ifdef INTELLINKED
    __itt_resume();
#endif
    for(int i = 0; i < 8000000; ++i)
      dum += foo();
#ifdef INTELLINKED
    __itt_pause();
#endif
    CALLGRIND_STOP_INSTRUMENTATION;

    // other computation

    std::cout << dum;
    return 0;
}
