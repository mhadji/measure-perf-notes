from Gaudi.Configuration import *
from Configurables import (RandGen,
                           TransformValue,
                           TellParity
                           )

ApplicationMgr().EvtMax = 10
ApplicationMgr().EvtSel = "NONE"

generateValue = RandGen()
computeParity = TransformValue()
printResult = TellParity()

ApplicationMgr().TopAlg.append(generateValue)
ApplicationMgr().TopAlg.append(computeParity)
ApplicationMgr().TopAlg.append(printResult)
