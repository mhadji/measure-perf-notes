#include "RandGen.h"

#include <GaudiKernel/MsgStream.h>
#include <iostream>
#include <ctime>
#include <unistd.h>

DECLARE_COMPONENT(RandGen)

int RandGen::operator()() const {
    info() << "[" << getpid()  << "] generate random value" << endmsg;
    std::srand( std::time(0) );
    return std::rand();
 }
