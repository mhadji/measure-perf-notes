#include "TellParity.h"
#include <GaudiKernel/MsgStream.h>

DECLARE_COMPONENT(TellParity)

void TellParity::operator()(const int& p) const {
    info() << (p ? "impair":"pair") << endmsg;
}
