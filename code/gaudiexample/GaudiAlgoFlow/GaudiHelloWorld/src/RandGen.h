#pragma once

#include <GaudiKernel/AnyDataHandle.h>
#include <GaudiAlg/Producer.h>

#include <string>

class RandGen
    : public Gaudi::Functional::Producer<int()> {

public:
    RandGen(const std::string& name, ISvcLocator* pSvcLocator)
        : Producer( name, pSvcLocator,
                    KeyValue( "OutputLocation", {"randvalue"}) ) {}

    int operator()() const override;
};
