#pragma once

#include <GaudiKernel/AnyDataHandle.h>
#include <GaudiAlg/Consumer.h>

#include <string>

class TellParity
    : public Gaudi::Functional::Consumer<void(const int&)>
{
public:
    TellParity(const std::string& name, ISvcLocator* pSvcLocator)
        : Consumer( name, pSvcLocator,
                    KeyValue( "InputLocation", {"modulo"}) ) {}

    void operator()(const int& p) const override;
};
