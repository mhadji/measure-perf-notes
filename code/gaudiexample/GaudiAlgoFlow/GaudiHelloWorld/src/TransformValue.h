#pragma once

#include <GaudiKernel/AnyDataHandle.h>
#include <GaudiAlg/Transformer.h>

class TransformValue
    : public Gaudi::Functional::Transformer<int(const int&)>
{
public:
    TransformValue(const std::string& name, ISvcLocator* pSvcLocator)
        : Transformer( name,
                       pSvcLocator,
                       KeyValue("Input1Loc", "randvalue"),
                       KeyValue("Ouput1Loc", "modulo") )
    { }

    int operator()(const int& value) const override;
};
