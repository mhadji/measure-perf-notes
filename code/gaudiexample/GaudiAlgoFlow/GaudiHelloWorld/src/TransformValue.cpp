#include "TransformValue.h"

#include <GaudiKernel/MsgStream.h>

DECLARE_COMPONENT(TransformValue)

int TransformValue::operator()(const int& value) const {
    info() << "get randvalue:[" << value << "]" << endmsg;
    return value%2;
}
