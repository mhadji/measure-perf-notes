from Gaudi.Configuration import *
from Configurables import (HelloWorldProducer,
                           HelloWorldConsumer)

ApplicationMgr().EvtMax = 1
ApplicationMgr().EvtSel = "NONE"

alg1 = HelloWorldProducer()
alg2 = HelloWorldConsumer()

ApplicationMgr().TopAlg.append(alg1)
ApplicationMgr().TopAlg.append(alg2)
