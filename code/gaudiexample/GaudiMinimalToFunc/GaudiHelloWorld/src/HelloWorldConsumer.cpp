#include "HelloWorldConsumer.h"
#include <GaudiKernel/MsgStream.h>

DECLARE_COMPONENT(HelloWorldConsumer)

void HelloWorldConsumer::operator()(const std::string& str) const {
    info() << "get string from tes: " << str << endmsg;
}
