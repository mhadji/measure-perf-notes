#include "HelloWorldProducer.h"
#include <GaudiKernel/MsgStream.h>

DECLARE_COMPONENT(HelloWorldProducer)

std::string HelloWorldProducer::operator()() const {
    info() << "put string into tes" << endmsg; 
    return "Hello World !";
}
