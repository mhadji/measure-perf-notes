#pragma once

#include <GaudiKernel/AnyDataHandle.h>
#include <GaudiAlg/Producer.h>

#include <string>

class HelloWorldProducer
    : public Gaudi::Functional::Producer<std::string()> {
public:
    /// Needs a constructor or delagating constructor
    HelloWorldProducer(const std::string& name, ISvcLocator* pSvcLocator)
        : Producer( name, pSvcLocator,
                    KeyValue( "OutputLocation", {"hello"}) ) {}

    std::string operator()() const override;
};
