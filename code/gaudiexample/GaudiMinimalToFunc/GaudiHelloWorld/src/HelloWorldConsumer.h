#pragma once

#include <GaudiKernel/AnyDataHandle.h>
#include <GaudiAlg/Consumer.h>

#include <string>

class HelloWorldConsumer
    : public Gaudi::Functional::Consumer<void(const std::string&)>
{
public:
    HelloWorldConsumer(const std::string& name, ISvcLocator* pSvcLocator)
        : Consumer( name, pSvcLocator,
                    KeyValue( "InputLocation", {"hello"}) ) {}

    void operator()(const std::string& input) const override;
};
