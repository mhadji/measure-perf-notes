#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <valgrind/callgrind.h>
#include <cmath>
#include <chrono>
#ifdef INTELLINKED
#include "ittnotify.h"
#endif
#define SIZE 10485760
#define NBR 10000

int main() {
    int * a = new int[SIZE];
    int * b = new int[NBR];
    srand(time(NULL));

    int c = 0, d = 0;
    for(int k = 0; k < NBR; ++k) {
        b[k] = rand() % SIZE;
    }
    for(int k = 0; k < SIZE; ++k) {
        a[k] = k;
    }

    CALLGRIND_START_INSTRUMENTATION;
#ifdef INTELLINKED
    __itt_resume();
#endif
    for(int k = 0; k < 1000; ++k) {
        c = 0;
        double dummy = 0.;
        for(int n = 0; n < NBR; ++n) {
            c += a[b[n]];
#ifdef IMPROVE_CPI
            if(n & 0x1)
                dummy += sqrt(n);
#endif
        }
        d += c + dummy;
    }
#ifdef INTELLINKED
    __itt_pause();
#endif
    CALLGRIND_STOP_INSTRUMENTATION;
    CALLGRIND_DUMP_STATS;

    printf("%d\n", d);
    return 0;
}
