#include <iostream>
#include <vector>
#include <ctime>
#include <cstdio>
#include <cstdlib>

#define NBR 50000000
#define F 100


struct Coord {
    float x, y, z;
};

struct Color {
    float r, g, b;
};

struct Vertex {
    int x, y, z;

    Vertex() : x(rand()%F), y(rand()%F), z(rand()%F) { }
};

int main(void) {
    srand(time(NULL));
    std::vector<Vertex> vf(NBR);

    /*
        for(unsigned int i=0; i < vf.size()-1; i++) {
            printf("[%d] :: %p - %p = %dB\n", i, &vf[i+1], &vf[i], (char*)&vf[i+1] - (char*)&vf[i]);
        }

    for(auto& v: vf)
        std::cout << v.x << " " << v.y << " " << v.z << std::endl;
    */

    int cpt=0;
    for(auto& v: vf) {
        if(v.x %25 == 0) {
            //std::cout << "[" << v.x << "]\n";
            cpt++;
        }
    }
    std::cout << cpt << std::endl;

    return 0;
}
