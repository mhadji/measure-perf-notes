#include <cstdio>
#include <cstdlib>
#include <ctime>

#define VALSIZE (1 << 25)
#define MINVALSIZE (VALSIZE-2)
#define MAX 15555

using type = int;

template<typename T>
T fsel(T a, T b) {
    bool mask = -bool(a < b);
    return (mask & a | mask & b);
}

template<typename T>
T min(T a, T b) {
#ifdef BRLESS
    return fsel(a, b);
#else
    return a < b ? a : b;
#endif
}

template<typename T>
T min(T a, T b, T c) {
    return min(a, min(b, c));
}

int main() {
    type* val = new type[VALSIZE];
    type* minval = new type[MINVALSIZE];

    srand(time(NULL));
    for(int i = 0; i < VALSIZE; ++i)
        val[i] = rand() % MAX;

    for(int i = 0; i < MINVALSIZE; ++i) {
        minval[i] = min(val[i],
                        val[i+1],
                        val[i+2]);
    }

    return 0;
}
