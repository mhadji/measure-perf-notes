#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <valgrind/callgrind.h>

#define SIZE 10485760
#define NBR 10000

int main() {
    std::cout << sizeof(int) << std::endl;
    int * a = new int[SIZE]; // size of 'a' in bytes is 2* size of L3 cache ()
    int * index = new int[NBR];
    srand(time(NULL));

    int c = 0, d = 0;
    for(int k = 0; k < NBR; ++k) {
#ifdef FORCE_CACHE_MISS
        index[k] = rand() % SIZE;
#else
        index[k] = k;
#endif
  }
  for(int k = 0; k < SIZE; ++k) {
      a[k] = k;
  }

  CALLGRIND_START_INSTRUMENTATION;
  for(int k = 0; k < 1000; ++k) {
      c = 0;
      for(int n = 0; n < NBR; ++n) {
          c += a[index[n]];
      }
      d += c;
  }
  CALLGRIND_STOP_INSTRUMENTATION;
  CALLGRIND_DUMP_STATS;

  printf("%d\n", d);
  return 0;
}
